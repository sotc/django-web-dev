# SOTC Django Session - 1

#### Python Installation

1. The first step in working with Django is to ensure that Python is installed on your system.

#### Virtual Environment Setup:

2. To maintain project-specific dependencies and avoid conflicts, we recommend using virtual environments. Virtual environments allow you to create isolated environments for different projects. You can install the `virtualenv` package using pip, the Python package manager, with the command: `pip install virtualenv`.

#### Creating a Virtual Environment:

3. Once `virtualenv` is installed, you can create a virtual environment for your Django project. Use the command: `virtualenv venv --python=python_version`. Replace `python_version` with the desired Python version, e.g., `python3.11`.

#### Activating the Virtual Environment:

4. To activate the virtual environment, we used the appropriate command based on your operating system.  command: `source ./venv/bin/activate`.

#### Django Installation:

5. With the virtual environment active, you can install Django using pip: `pip install django`.

#### Project Initialization:

6. Navigate to the desired directory where you want to create your Django project. Use the command: `django-admin startproject django_sotc`. Replace `django_sotc` with the name of your project.

#### Running the Development Server:

7. Change to the project directory using the command: `cd projectname`. Once inside the project directory, start the development server with the command: `python manage.py runserver`. This will launch the server, and you can view your project in a web browser at the specified address.

#### Creating an App:

8. Django follows a modular approach, and an app is a self-contained module within a Django project. To create an app, use the command: `python manage.py startapp day1`. Replace `day1` with the desired app name. Then add your app name in the `settings.py` under `INSTALLED_APPS` list

#### Database Migration:

9. Django's migration system allows you to manage changes to your models and synchronize them with the database. Apply initial migrations using the command: `python manage.py migrate`.

#### Creating a Superuser:

10. To access the Django administration interface, create a superuser account. Execute the command: `python manage.py createsuperuser` and follow the prompts to provide a username and password for the superuser.

```bash
## BASH - Ubuntu 22.04 ##

# Create your working directory
mkdir django_sotc
cd django_sotc

# Setting up virtual environment
pip install virtualenv
virtualenv venv
source ./venv/bin/activate

# setting up our dj project
pip install django
django-admin startproject django_sotc
mv django_sotc django_sotc1
mv django_sotc1/* ./
rm -r django_sotc1

# write our requirements.txt
pip freeze > requirements.txt

# To a development runserver
./manage.py runserver

# create our first application
./manage.py startapp day1

# Add your app name in django_sotc/settings.py under INSTALLED_APPS list

# migrate your models to database
./manage.py makemigrations
./manage.py migrate

# Create a superuser
./manage.py createsuperuser
```

## CRUD operations

CRUD operations in Django refer to the basic operations that can be performed on a database: Create, Read, Update, and Delete. These operations allow you to interact with the data stored in your database through your Django web application.

Before getting into CRUD Operations we should define for models in Django
Models are nothing but model maps in a single database table

Let's build a model in Django:
   - Table name `crud`
   - field name `name` of datatype `string`
   - field name `age` of datatype `integer`
   - field name `steam` of datatype `string`

   ```python3.11
   # day1/models.py

   from django.db import models

   # Create your models here.
   class Crud(models.Model):
      name = models.CharField(max_length=30)
      age = models.IntegerField()
      steam = models.CharField(max_length=30)

      def __str__(self):
         return self.name
   ```

Now, let's go through each step of the CRUD operations in Django:

1. #### Create (C): 

    The create operation involves adding new data to the database. In Django, you typically create a new record by following these steps:

   - Define a model: A model is a Python class that represents a database table. It defines the fields and their types.
   - Create an instance of the model: Instantiate the model class to create a new object with the desired data.
   - Save the object: Call the `save()` method on the object to save it to the database.

   ```python3.11
   from day1.models import Crud

   # Example 1: Using create() function
   Crud.objects.create(name='John', age=20, steam='Computer Science')

   # Example 2: Using save() function
   crud = Crud()
   crud.name = 'John'
   crud.age = 20
   crud.steam = 'Computer Science'
   crud.save()
   ```
2. #### Read (R):

    The read operation involves retrieving data from the database. In Django, you can perform various read operations:

   - Retrieve all records: Use the model's `objects.all()` method to get all records from the table.
   - Filter records: Use the `objects.filter()` method to retrieve records that meet certain criteria.
   - Retrieve a single record: Use methods like `objects.get()` to retrieve a single record based on specific conditions.

   ```python3.11
   from day1.models import Crud

   # Example 1: Retrieve all objects
   Crud.objects.all()

   # Example 2: Filtering records
   Crud.objects.filter(age=20)

   # Example 3: Retrieve a single record
   Crud.objects.get(name='John') # get() params value should be unique
   ```
3. #### Update (U): 

    The update operation involves modifying existing data in the database. In Django, you can update records by following these steps:

   - Retrieve the record: Use filtering or other methods to retrieve the specific record you want to update.
   - Modify the data: Change the desired fields or attributes of the retrieved object.
   - Save the changes: Call the `save()` method on the object to save the modifications to the database.

   ```python3.11
   from day1.models import Crud

   # Here we will update steam 'Computer Science' -> 'Data Science' of John
   query = Crud.objects.get(name='John') # get() params value should be unique
   query.steam = 'Data Science'
   query.save()
   ```
4. #### Delete (D):

   The delete operation involves removing data from the database. In Django, you can delete records by following these steps:


   - Call the `delete()` method: Invoke the `delete()` method on the retrieved object to remove it from the database.
   - Retrieve the record: Use filtering or other methods to retrieve the specific record you want to delete.

   ```python3.11
   from day1.models import Crud

   # Let's delete the record John
   query = Crud.object.get(name='John') # get() params value should be unique
   query.delete()
   ```

These are the basic steps involved in performing CRUD operations in Django. Each operation can be implemented using Django's ORM (Object-Relational Mapping) capabilities, which abstract the underlying database and provide a convenient way to interact with the data using Python objects.
