from django.db import models
from django.contrib.auth.models import AbstractUser, PermissionsMixin

# Create your models here.
class SotcUsers(AbstractUser, PermissionsMixin):
    dob = models.DateField(null= True, blank=True)
    gender = models.CharField(max_length=30, null= True, blank=True)
    contact = models.IntegerField(null= True, blank=True)
    address = models.TextField(null=True, blank=True)


# Username
# password
# mailid
# contact
# First name, Last name
# Gender
# Address
# DOB
