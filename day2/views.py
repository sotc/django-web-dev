from django.shortcuts import render, redirect, resolve_url
from day2.models import SotcUsers
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


# Create your views here.

def registration(request):
    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        c_password = request.POST['confirm_password']
        if password == c_password:
            SotcUsers.objects.create(username=username, email=email, password=make_password(password)) 
            return redirect(resolve_url('signin'))
        else:
            return render(request, template_name='register.html')
    else:
        return render(request, template_name='register.html')


def signin(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request=request, username=username, password=password)
        if user:
            login(request, user)
            return redirect(resolve_url('home'))
        else:
            return render(request, template_name='login.html', context={'error':'Invalid username/password'})
    else:
        return render(request, template_name='login.html')

def signout(request):
    logout(request)
    return redirect(resolve_url('signin'))

@login_required(login_url='signin/')
def home(request):
    return render(request, 'home.html')